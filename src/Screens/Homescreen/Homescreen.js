import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

export default class Homescreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.route.params.token,
    };
  }

  componentDidMount() {
    console.log('INI ADALAH TOKEN : ', this.props.route.params.token);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('GetAllChecklist', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentText}>Get All Checklist</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('CreateNewChecklist', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentTextSeparator}>
              Create New Checklist
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('DeleteChecklist', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentText}>Delete Checklist</Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('GetAllItem', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentText}>Get All Checklist item</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('CreateNewChecklist', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentTextSeparator}>
              Update Status Item
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('DeleteChecklist', {
                token: this.state.token,
              })
            }
            style={styles.button}>
            <Text style={styles.contentText}>Delete Item</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-evenly',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  contentText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  contentTextSeparator: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    borderLeftColor: 'white',
    borderRightColor: 'white',
    borderLeftWidth: 2,
    borderRightWidth: 2,
  },
  button: {
    height: 120,
    width: 120,
    backgroundColor: '#4287f5',
    justifyContent: 'center',
  },
});
