import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';

export default class CreateNewChecklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: this.props.route.params.token,
      nama: '',
    };
  }
  componentDidMount() {
    console.log('INI ADALAH TOKEN : ', this.props.route.params.token);
  }

  submit = () => {
    axios
      .post(
        `http://94.74.86.174:8080/api/checklist`,
        {
          name: this.state.nama,
        },
        {
          headers: {Authorization: `Bearer ${this.state.token}`},
        },
      )
      .then(response => {
        console.log('INI ADALAH RESPON CREATE NEW CHECKLIST : ', response.data);
        if (
          response.data.message === 'Proses save berhasil' &&
          response.data.statusCode === 2000
        ) {
          alert(response.data.message);
          this.setState({nama: ''});
        } else {
          alert('GAGAL');
        }
      })
      .catch(function (error) {
        console.log(error);
        alert('Gagal');
      });
  };

  render() {
    const {name} = this.state;
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={styles.textInputContainer}>
          <TextInput
            onChangeText={nama => {
              this.setState({nama});
            }}
            placeholder="Nama Checklist Baru"
            maxLength={40}
            autoCorrect={false}
            style={styles.textInputStyle}></TextInput>
        </View>
        <TouchableOpacity
          style={{
            backgroundColor: 'rgba(37, 37, 34, 0.22)',
            height: 70,
            width: 180,
            alignItems: 'center',
            justifyContent: 'center',
            marginBottom: hp('2%'),
            marginHorizontal: wp('5%'),
            borderRadius: 20,
            alignSelf: 'center',
          }}
          onPress={() => this.submit()}>
          <Text style={{fontWeight: 'bold', fontSize: 20}}>SUBMIT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  textInputContainer: {
    borderColor: 'black',
    borderWidth: 0.5,
    // backgroundColor: "red",
    paddingHorizontal: wp('5%'),
    // justifyContent: "center",
    alignItems: 'center',
    height: hp('5%'),
    marginBottom: hp('5%'),
    marginHorizontal: wp('5%'),
  },
  dropDownContainer: {
    // backgroundColor: "blue",
    height: hp('13%'),
    width: wp('100%'),
    // justifyContent: "space-around",
    paddingHorizontal: wp('5%'),
  },
  dropDownStyle: {
    marginTop: hp('2%'),
    height: hp('5%'),
    // backgroundColor: "red",
    width: wp('85%'),
  },
  itemDropDown: {
    justifyContent: 'flex-start',
  },
  placeholderStyle: {
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    color: 'black',
  },
  labelStyle: {
    textAlign: 'left',
    color: 'black',
    fontFamily: 'Poppins-Regular',
  },
});
