import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  Homescreen,
  DropdownProvince,
  DummyApi,
  OpenCamera,
  Login,
  Register,
  GetAllChecklist,
  CreateNewChecklist,
  DeleteChecklist,
  GetAllItem
} from '../Screens';
const Stack = createNativeStackNavigator();

function Routes() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Homescreen" component={Homescreen} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="GetAllChecklist" component={GetAllChecklist} />
        <Stack.Screen name="CreateNewChecklist" component={CreateNewChecklist} />
        <Stack.Screen name="DeleteChecklist" component={DeleteChecklist} />
        <Stack.Screen name="GetAllItem" component={GetAllItem} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
